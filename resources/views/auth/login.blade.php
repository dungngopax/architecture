@extends('layouts.auth')

@section('content')
<form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
    @csrf
    <section class="loginBox posR ovh">
        <header class="txtC ovh txtB">
            <a href="#" class="logo dpib ovh" tabindex="-1">
                <img src="https://cdn-app.kiotviet.vn/retailler/Content/kiotvietLogo.png" alt="KiotViet" title="KiotViet">
            </a>
        </header>

        <section class="loginFr ovh vi-VN">
            <h3>{{ __('Đăng nhập') }}</h3>
            
            <div class="mb15 posR">
                <label>Username / {{ __('E-Mail') }}</label>
                <input id="login" type="text" class="form-control{{ $errors->has('login') ? ' is-invalid' : '' }}" name="login" value="{{ old('login') }}" required autofocus>
                
                @if ($errors->has('login'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('login') }}</strong>
                    </span>
                @endif
                
            </div>
            <div class="posR mb15">
                <label>Mật khẩu</label>
                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                
                @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>

            <div class="pretty p-icon p-smooth">
                <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                <div class="state p-success">
                    <i class="icon fa fa-check"></i>
                    <label>Duy trì đăng nhập</label>
                </div>
            </div>

            <div class="row mt-3">
                <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-primary mr-3"><i class="fas fa-list-alt"></i> {{ __('Quản lý') }}</button>
                    <button type="submit" class="btn btn-success"><i class="fas fa-shopping-cart"></i> {{ __('Bán hàng') }}</button>
                </div>
            </div>
        </section>
        
        <div class="other"><i class="fa fa-phone-square"></i>&nbsp;&nbsp; Hỗ trợ: 18006162</div>
    </section>
</form>
@endsection
