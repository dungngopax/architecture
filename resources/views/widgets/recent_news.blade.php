@if (count($records) > 0)
<div class="section padding-tb-100px section-ba-3">
    <div class="container">
        <!-- Title -->
        <div class="section-title margin-bottom-40px">
            <div class="row justify-content-center">
                <div class="col-lg-7">
                    <div class="icon text-main-color"><i class="fa fa-bookmark-o"></i></div>
                    <div class="h2">Tin tức mới nhất</div>
                    <div class="des">In quis luctus dolor. Sed ac libero arcu. Phasellus vulputate ultrices augue, eget feugiat lectus efficitur in. Nulla non pharetra justo. Nunc viverra consectetur bibendum. </div>
                </div>
            </div>
        </div>
        <!-- // End Title -->

        <div class="row">
            @foreach ($records as $item)
                @php
                    $url = url('tin-tuc-su-kien/' . $item->newsEventsCats['slug'] . '/' . $item->slug);
                @endphp
                
                <div class="col-lg-4 col-md-6 sm-mb-35px">
                    <div class="blog-item">
                        <div class="img">
                            <a href="{{$url}}">
                                <img src="{{$item->resizeImage($item->thumbnail, 410)}}" alt="">
                            </a>
                            <a href="{{$url}}" class="date">
                            <span class="day">{{$item->created_at->format('d')}}</span>
                                <span class="month">{{$item->created_at->format('m-Y')}}</span>
                            </a>
                        </div>
                        <a href="{{$url}}" class="title">{{$item->title}}</a>
                    </div>
                </div>

            @endforeach

        </div>

    </div>
</div>
@endif