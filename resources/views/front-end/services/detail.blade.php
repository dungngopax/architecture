@extends('layouts.app')

@section('title', $record->title)

@section('content')
    <div class="page-title">
        <div class="container">
            <div class="padding-tb-120px">
                <h1 class="mt-5">Dịch vụ</h1>
                @if ($cat)
                    <ol class="breadcrumb">
                        <li class="active">{{$cat->title}}</li>
                    </ol>    
                @endif
            </div>
        </div>
    </div>

    <div class="section padding-tb-50px section-ba-3">
        <div class="container">

            <div class="row">
                
                <div class="col-md-9">

                    <div class="post-standard">
                        <div class="thum-img">
                            <img src="http://placehold.it/950x400" alt="">
                        </div>

                        <a href="#" class="title">{{$record->title}}</a>
                        
                        <div class="meta-out margin-bottom-20px">
                            <ul class="meta">
                                <li> <span class="icon icon_clock_alt"></span> {{$record->created_at->format('d-m-Y H:i:s')}} </li>
                                <li> <span class="icon icon_profile"></span><a href="#">Admin</a> </li>
                            </ul>
                        </div>

                        <div class="entry-content">{!! $record->content !!}</div>

                    <div class="fb-comments mt-50" data-href="{{url('dich-vu/' . $cat->slug . '/' . $record->slug)}}" width="100%" data-numposts="5"></div>
                    </div>

                </div>
                
                <div class="col-md-3">

                    @if (count($cats) > 0)
                        <div class="widget sidebar-wideget widget_categories">
                            <div class="sidebar-title">
                                <h2>Danh mục</h2>
                            </div>
                            <ul>
                                @foreach ($cats as $item)
                                    <li class="{{($cat->id == $item->id) ? 'active' : ''}}"><a href="{{url('dich-vu/' . $item->slug)}}">{{$item->title}}</a></li>    
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    @if (count($recordsRel) > 0)
                        <div class="widget sidebar-wideget">
                            <div class="sidebar-title">
                                <h2>Bài viết liên quan</h2>
                            </div>
                            <div class="last-post">
                                @foreach ($recordsRel as $item)
                                    @php
                                        $url = url('dich-vu/' . $cat->slug . '/' . $item->slug);
                                    @endphp
                                    <div class="item">
                                    <div class="img"><a href="{{$url}}"><img src="{{$item->resizeImage($item->thumbnail, 265)}}" alt=""></a></div>
                                        <a class="title" href="{{$url}}">{{$item->title}}</a>
                                        <div class="date"><i class="fa fa-clock-o"></i> {{$item->created_at->format('d-m-Y H:i:s')}}</div>
                                    </div>    
                                @endforeach
                            </div>
                        </div>
                    @endif

                </div>

            </div>

        </div>
    </div>

    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v3.2&appId=163805121003836&autoLogAppEvents=1';
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
@endsection
