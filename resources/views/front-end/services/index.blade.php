@extends('layouts.app')

@section('title', 'Dịch vụ')

@section('content')
    <div class="page-title">
        <div class="container">
            <div class="padding-tb-120px">
                <h1 class="mt-5">Dịch vụ</h1>
                @if ($cat)
                    <ol class="breadcrumb">
                        <li class="active">{{$cat->title}}</li>
                    </ol>    
                @endif
            </div>
        </div>
    </div>

    <div class="section padding-tb-50px section-ba-3">
        <div class="container">

            <div class="row">

                @foreach ($records as $item)
                    @php
                        $url = url('dich-vu/' . $item->servicesCats['slug'] . '/' . $item->slug);
                    @endphp
                    
                
                    <div class="col-lg-4 col-md-6 sm-mb-35px mt-4">
                        <div class="blog-item">
                            <div class="img">
                                <a href="{{$url}}">
                                    <img src="{{$item->resizeImage($item->thumbnail, 410)}}" alt="">
                                </a>
                                <a href="{{$url}}" class="date">
                                <span class="day">{{$item->created_at->format('d')}}</span>
                                    <span class="month">{{$item->created_at->format('m-Y')}}</span>
                                </a>
                            </div>
                            <a href="{{$url}}" class="title">{{$item->title}}</a>
                        </div>
                    </div>

                @endforeach

            </div>

        </div>
    </div>
@endsection
