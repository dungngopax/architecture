@extends('layouts.app')

@section('title', 'Giới thiệu')

@section('content')
    <div class="page-title">
        <div class="container">
            <div class="padding-tb-120px">
                <h1 class="mt-5">Giới thiệu</h1>
            </div>
        </div>
    </div>

    <div class="nile-about-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 sm-mb-45px">

                    <div class="section-title-right text-main-color clearfix">
                        <div class="icon"><img src="{{asset('assets/icons/title-icon-1.png')}}" alt=""></div>
                        <h2 class="title-text">Who We Are ?</h2>
                    </div>
                    <div class="about-text margin-tb-25px">
                        <h4>25 years of experience in Logistics services</h4>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris
                        <br> consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ...
                    </div>
                    <a href="#" class="nile-bottom sm">Read More</a>

                </div>
                <div class="col-lg-6">
                    <img src="{{asset('assets/img/about-1.jpg')}}" alt="">
                </div>
            </div>
        </div>
    </div>

    <div class="call-action ba-2">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 padding-tb-15px">
                    <h2>Unbeatable Trucking and Transport Services</h2>
                    <div class="text">Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</div>
                </div>
                <div class="col-lg-5">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 sm-mb-45px">
                            <a href="#" class="action-bottom layout-1">
                                <img src="{{asset('assets/icons/small-icon-1.png')}}" alt="">
                                <h4>Tell Friend</h4>
                            </a>
                        </div>
                        <div class="col-lg-4 col-md-4 sm-mb-45px">
                            <a href="#" class="action-bottom layout-1">
                                <img src="{{asset('assets/icons/small-icon-2.png')}}" alt="">
                                <h4>Read More</h4>
                            </a>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <a href="#" class="action-bottom layout-1">
                                <img src="{{asset('assets/icons/small-icon-3.png')}}" alt="">
                                <h4>Contact Us</h4>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="nile-about-section">
        <div class="container">
            <!-- Title -->
            <div class="section-title margin-bottom-40px">
                <div class="row justify-content-center">
                    <div class="col-lg-7">
                        <div class="icon text-main-color"><i class="fa fa-truck"></i></div>
                        <div class="h2">Our Service</div>
                        <div class="des">In quis luctus dolor. Sed ac libero arcu. Phasellus vulputate ultrices augue, eget feugiat lectus efficitur in. Nulla non pharetra justo. Nunc viverra consectetur bibendum. </div>
                    </div>
                </div>
            </div>
            <!-- // End Title -->

            <div class="row">

                <div class="col-lg-4">
                    <div class="service-icon-box">
                        <div class="icon"><img src="{{asset('assets/icons/service-dark-1.png')}}" alt=""></div>
                        <a href="#" class="title h2">Cargo Transportation</a>
                        <div class="des">Ut elit tellus, luctus nec magna mattis et, pulvinar dapibus lorem leo ultricies et vitae enim.</div>
                    </div>
                    <div class="service-icon-box">
                        <div class="icon"><img src="{{asset('assets/icons/service-dark-2.png')}}" alt=""></div>
                        <a href="#" class="title h2">Air Freight</a>
                        <div class="des">Ut elit tellus, luctus nec magna mattis et, pulvinar dapibus lorem leo ultricies et vitae enim.</div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="text-center sm-mb-45px">
                        <img src="{{asset('assets/img/about-2.jpg')}}" alt="" class="border-radius-500">
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="service-icon-box">
                        <div class="icon"><img src="{{asset('assets/icons/service-dark-3.png')}}" alt=""></div>
                        <a href="#" class="title h2">Ocean Freight</a>
                        <div class="des">Ut elit tellus, luctus nec magna mattis et, pulvinar dapibus lorem leo ultricies et vitae enim.</div>
                    </div>
                    <div class="service-icon-box">
                        <div class="icon"><img src="{{asset('assets/icons/service-dark-4.png')}}" alt=""></div>
                        <a href="#" class="title h2">Packaging & Storage</a>
                        <div class="des">Ut elit tellus, luctus nec magna mattis et, pulvinar dapibus lorem leo ultricies et vitae enim.</div>
                    </div>
                </div>



            </div>


            <div class="text-center margin-top-35px">
                <a href="#" class="nile-bottom md">Show all <i class="fa fa-arrow-right"></i> </a>
            </div>

        </div>
    </div>
@endsection
