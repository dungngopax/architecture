@php
    use App\Models\NewsEventsCats;
    use App\Models\ServicesCats;

    $newsEventsCats = NewsEventsCats::where('status', 1)->orderBy('position', 'desc')->get();
    $servicesCats = ServicesCats::where('status', 1)->orderBy('position', 'desc')->get();

    $seg1 = Request::segment(1);
    $seg2 = Request::segment(2);
    
@endphp

<header>
    <div id="fixed-header-dark" class="header-output fixed-header">
        <div class="header-output">
            <div class="container header-in">

                <!-- Up Head -->
                <div class="up-head d-none d-lg-block">
                    <div class="row">
                        <div class="col-xl-8 col-lg-12">
                            <div class="row">
                                <div class="col-md-4"><i class="fa fa-phone margin-right-10px"></i> 00222 123 333 019</div>
                                <div class="col-md-4"><i class="fa fa-envelope-o margin-right-10px"></i> info@your-site.com</div>
                                <div class="col-md-4"><i class="fa fa-map-marker margin-right-10px"></i> 1105 Saudi Arabia - Street, CA</div>
                            </div>
                        </div>
                        <div class="col-xl-4 d-none d-xl-block">
                            <div class="row">
                                <div class="col-lg-6"></div>

                                <div class="col-lg-6">
                                    <!--  Social -->
                                    <ul class="social-media list-inline text-right margin-0px text-white">
                                        <li class="list-inline-item"><a class="facebook" href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                        <li class="list-inline-item"><a class="youtube" href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                                        <li class="list-inline-item"><a class="linkedin" href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                        <li class="list-inline-item"><a class="google" href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                                        <li class="list-inline-item"><a class="twitter" href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                        <li class="list-inline-item"><a class="rss" href="#"><i class="fa fa-rss" aria-hidden="true"></i></a></li>
                                    </ul>
                                    <!-- // Social -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- Menu --}}
                <div class="position-relative">
                    <div class="row">
                        {{-- Logo --}}
                        <div class="col-lg-3 col-md-12">
                            <a id="logo" href="{{url('')}}" class="d-inline-block margin-tb-15px">
                                <img src="{{asset('assets/img/logo-1.png')}}" alt="">
                            </a>
                            <a class="mobile-toggle padding-15px background-second-color border-radius-3" href="#"><i class="fa fa-bars"></i></a>
                        </div>

                        <div class="col-lg-7 col-md-12 position-inherit">
                            <ul id="menu-main" class="nav-menu float-xl-left text-lg-center link-padding-tb-25px white-link dropdown-dark">
                                <li class="{{empty($seg1) ? 'active' : ''}}"><a href="{{url('/')}}">Trang chủ</a></li>
                                <li class="{{($seg1 == 'gioi-thieu') ? 'active' : ''}}"><a href="{{url('gioi-thieu')}}">Giới thiệu</a></li>
                                
                                <li class="{{($seg1 == 'dich-vu') ? 'active' : ''}} has-dropdown">
                                    <a href="{{url('dich-vu')}}">Dịch vụ</a>

                                    @if (count($servicesCats) > 0)
                                        <ul class="sub-menu">
                                            @foreach ($servicesCats as $item)
                                                <li class="{{($seg2 == $item->slug) ? 'active' : ''}}">
                                                    <a href="{{url('dich-vu/' . $item->slug)}}">{{$item->title}}</a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </li>
                                
                                <li class="{{($seg1 == 'tin-tuc-su-kien') ? 'active' : ''}} has-dropdown">
                                    <a href="{{url('tin-tuc-su-kien')}}">Tin tức & Sự kiện</a>

                                    @if (count($newsEventsCats) > 0)
                                        <ul class="sub-menu">
                                            @foreach ($newsEventsCats as $item)
                                                <li class="{{($seg2 == $item->slug) ? 'active' : ''}}">
                                                    <a href="{{url('tin-tuc-su-kien/' . $item->slug)}}">{{$item->title}}</a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </li>
                                
                                <li class="{{($seg1 == 'lien-he') ? 'active' : ''}}"><a href="{{url('lien-he')}}">Liên hệ</a></li>
                            </ul>
                        </div>

                        <div class="col-lg-2 col-md-12  d-none d-lg-block">
                            <a href="#search" class="btn btn-sm border-radius-30 margin-tb-20px text-white  background-main-color  box-shadow float-right padding-lr-20px margin-left-30px d-block">
                                <i class="fa fa-search"></i> Search
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>