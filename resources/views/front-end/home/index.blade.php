@extends('layouts.app')

@section('content')
    @include('front-end.home.rev_slider')
    @include('front-end.home.about-us')
    @include('front-end.home.our-service')
    
    @asyncWidget('RecentNews')
@endsection
