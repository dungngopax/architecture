<div class="section padding-tb-100px section-ba-1">
    <div class="container">
        <!-- Title -->
        <div class="section-title margin-bottom-40px">
            <div class="row justify-content-center">
                <div class="col-lg-7">
                    <div class="icon text-main-color"><i class="fa fa-truck"></i></div>
                    <div class="h2">Our Service</div>
                    <div class="des">In quis luctus dolor. Sed ac libero arcu. Phasellus vulputate ultrices augue, eget feugiat lectus efficitur in. Nulla non pharetra justo. Nunc viverra consectetur bibendum. </div>
                </div>
            </div>
        </div>
        <!-- // End Title -->

        <div class="row">

            <div class="col-lg-3 col-md-6">
                <div class="service-icon-box">
                    <a href="#" class="title h2">Cargo Transportation</a>
                    <div class="des">Ut elit tellus, luctus nec magna mattis et, pulvinar dapibus lorem leo ultricies et vitae enim.</div>
                </div>
            </div>


            <div class="col-lg-3 col-md-6">
                <div class="service-icon-box">
                    <a href="#" class="title h2">Air Freight</a>
                    <div class="des">Ut elit tellus, luctus nec magna mattis et, pulvinar dapibus lorem leo ultricies et vitae enim.</div>
                </div>
            </div>


            <div class="col-lg-3 col-md-6">
                <div class="service-icon-box">
                    <a href="#" class="title h2">Ocean Freight</a>
                    <div class="des">Ut elit tellus, luctus nec magna mattis et, pulvinar dapibus lorem leo ultricies et vitae enim.</div>
                </div>
            </div>


            <div class="col-lg-3 col-md-6">
                <div class="service-icon-box">
                    <a href="#" class="title h2">Packaging & Storage</a>
                    <div class="des">Ut elit tellus, luctus nec magna mattis et, pulvinar dapibus lorem leo ultricies et vitae enim.</div>
                </div>
            </div>
        </div>


        <div class="text-center">
            <a href="#" class="nile-bottom md">Show all <i class="fa fa-arrow-right"></i> </a>
        </div>

    </div>
</div>