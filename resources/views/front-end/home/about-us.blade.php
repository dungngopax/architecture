<div class="nile-about-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">

                <div class="section-title-right text-main-color clearfix">
                    <div class="icon"><img src="{{asset('assets/icons/title-icon-1.png')}}" alt=""></div>
                    <h2 class="title-text">Who We Are ?</h2>
                </div>
                <div class="about-text margin-tb-25px">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris</div>
                <a href="#" class="nile-bottom sm">Read More</a>


                <div id="accordion" class="nile-accordion margin-top-80px sm-mb-45px">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-block btn-link active" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><i class="fa fa-info-circle"></i> Why us ?</button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-block btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"><i class="fa fa-truck"></i> Explore our Facilities</button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                                Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-block btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree"><i class="fa fa-plane"></i> Warehousing Solution</button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                            <div class="card-body">
                                Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation </div>
                        </div>
                    </div>
                </div>



            </div>
            <div class="col-lg-6">

                <div class="row">
                    <div class="col-sm-6">
                        <a href="#"><img src="{{asset('assets/img/cart-2.jpg')}}" alt=""></a>
                    </div>
                    <div class="col-sm-6">
                        <div class="cart-service background-main-color">
                            <div class="icon"><img src="{{asset('assets/icons/service-light-2.png')}}" alt=""></div>
                            <h2>Air Freight</h2>
                            <hr>
                            <div class="text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="cart-service background-main-color">
                            <div class="icon"><img src="{{asset('assets/icons/service-light-3.png')}}" alt=""></div>
                            <h2>Air Freight</h2>
                            <hr>
                            <div class="text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <a href="#"><img src="{{asset('assets/img/cart-1.jpg')}}" alt=""></a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="call-action ba-1">
    <div class="container">
        <div class="row">
            <div class="col-lg-7 padding-tb-15px">
                <h2>Unbeatable Trucking and Transport Services</h2>
                <div class="text">Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</div>
            </div>
            <div class="col-lg-5">
                <div class="row">
                    <div class="col-lg-4 col-md-4 sm-mb-45px">
                        <a href="#" class="action-bottom layout-1">
                            <img src="{{asset('assets/icons/small-icon-1.png')}}" alt=""> 
                            <h4>Tell Friend</h4>
                        </a>
                    </div>
                    <div class="col-lg-4 col-md-4 sm-mb-45px">
                        <a href="#" class="action-bottom layout-1">
                            <img src="{{asset('assets/icons/small-icon-2.png')}}" alt=""> 
                            <h4>Read More</h4>
                        </a>
                    </div>
                    <div class="col-lg-4 col-md-4">
                        <a href="#" class="action-bottom layout-1">
                            <img src="{{asset('assets/icons/small-icon-3.png')}}" alt=""> 
                            <h4>Contact Us</h4>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>