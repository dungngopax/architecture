<div class="box-body table-responsive no-padding">
    <table class="table table-hover">
        <tr>
            <th>ID</th>
            <th>Fullname</th>
            <th>Email</th>
            <th class="text-center">IP</th>
            <th class="text-center">Datetime</th>
            <th></th>
        </tr>

        @if (count($records) > 0)
            @foreach ($records as $item)
                <tr>
                    <td>{{$item->id_display}}</td>
                    <td>{{$item->fullname}}</td>
                    <td>{{$item->email}}</td>
                    <td class="text-center">{{$item->ip}}</td>
                    <td class="text-center">{{$item->created_at->format('d-m-Y H:i:s')}}</td>
                    <td class="text-center">
                        <span data-action="{{$mod}}/{{$item->id}}" data-target="#modal-edit-{{$mod}}" class="label pointer label-success ajax-modal">Edit</span>
                        <span data-action="{{$mod}}/{{$item->id}}" data-target="#modal-edit-{{$mod}}" class="label pointer label-danger delete-item ml-5px">Delete</span>
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="5" class="text-center">No data</td>
            </tr>
        @endif
    </table>
</div>
<div class="text-center wrap-pagination">{{$records->links()}}</div>