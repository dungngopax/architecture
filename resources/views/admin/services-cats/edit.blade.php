<form action="{{url('admin/'.$mod.'/update')}}" method="post" id="form-edit-{{$mod}}">
    {{ csrf_field() }}
    {{ method_field('PUT') }}

    <input type="hidden" name="id" value="{{$record->id}}">

    <!-- Modal Header -->
    <div class="modal-header">
        <h4 class="modal-title">{{empty($record->id)?'Create':'Update'}} {{$title}}</h4>
        <button type="button" class="close btn-close-modal" data-dismiss="modal">&times;</button>
    </div>

    <!-- Modal body -->
    <div class="modal-body">
        <div class="tab-content mt-3">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            @include('admin.components.inputs.text', ['label'=>'Title', 'name'=>'title', 'value'=>$record->title, 'required'=>true, 'colLeft' => 12])
                        </div>
                    </div>
        
                    <div class="row">
                        <div class="col-md-6">
                            @include('admin.components.inputs.text', ['type' =>'number', 'label'=>'Position', 'name'=>'position', 'value'=>$record->position??0, 'required'=>true, 'colLeft' => 4, 'className' => 'mb-0'])
                        </div>
                        <div class="col-md-6">
                            @include('admin.components.inputs.select', ['label'=>'Status', 'name'=>'status', 'value'=>$record->status ?? 1, 'data' => $status, 'required'=>true, 'colLeft' => 4, 'className' => 'mb-0'])
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal footer -->
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button data-form-target="#form-edit-{{$mod}}" type="button" class="btn btn-primary btn-submit" auto-close-modal="true" data-form-method ="PUT">OK</button>
    </div>
</form>

<script>
    $(document).ready(function () {

        if($(".dropzone-file").length > 0){
            $(".dropzone-file").dropzone({
                url: "/admin/upload-file",
                sending: function(file, xhr, formData) {
                    formData.append("_token", CSRF_TOKEN);
                },
                success: function(file, res){
                    if(res.status == 200){
                        $('#img-preview img').attr('src', res.url);
                        $('input[name=thumbnail]').val(res.pathTmpFile);
                        $(".close-img-preview").removeClass('hide');
                    }else{
                        $('#img-preview img').attr('src', urlNoImage);
                        $('input[name=thumbnail]').val('');
                        $(".close-img-preview").addClass('hide');
                    }
                }
            });
        }

    });
</script>
    