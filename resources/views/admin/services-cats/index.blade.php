@extends('layouts.admin')

@section('title', $title)

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            {{$title}}
            <small>Danh sách</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
            <li class="active">{{$title}}</li>
        </ol>
    </section>
      
    <!-- Main content -->
    <section class="content">

        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    @include('admin.' . $mod . '.filter')
                    @include('admin.' . $mod . '.list')
                </div>
            </div>
        </div>

    </section>
@endsection

@push('modals')
<div class="modal fade" id="modal-edit-{{$mod}}">
    <div class="modal-dialog width-60 modal-dialog-centered">
        <div class="modal-content"></div>
    </div>
</div>
@endpush