@php
    $seg2 = Request::segment(2);
@endphp

<aside class="main-sidebar">
    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
            <img src="{{asset('admin/dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
            <p>Alexander Pierce</p>
            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            
            <li class="{{($seg2 == 'news-events-cats') ? 'active' : ''}}">
                <a href="{{url('admin/news-events-cats')}}">
                    <i class="fa fa-dashboard"></i> <span>News - Events Categories</span>
                </a>
            </li>

            <li class="{{($seg2 == 'news-events') ? 'active' : ''}}">
                <a href="{{url('admin/news-events')}}">
                    <i class="fa fa-dashboard"></i> <span>News - Events</span>
                </a>
            </li>

            <li class="{{($seg2 == 'services-cats') ? 'active' : ''}}">
                <a href="{{url('admin/services-cats')}}">
                    <i class="fa fa-dashboard"></i> <span>Service Categories</span>
                </a>
            </li>

            <li class="{{($seg2 == 'services') ? 'active' : ''}}">
                <a href="{{url('admin/services')}}">
                    <i class="fa fa-dashboard"></i> <span>Services</span>
                </a>
            </li>

            <li class="{{($seg2 == 'contacts') ? 'active' : ''}}">
                <a href="{{url('admin/contacts')}}">
                    <i class="fa fa-dashboard"></i> <span>Contacts</span>
                </a>
            </li>
        </ul>

    </section>
</aside>