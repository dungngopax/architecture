
<div class="row form-group {{$className ?? ''}} fg-{{$name}}">
    <div class="col-md-12">

        <div class="pretty p-default p-round d-flex mt-3">
            <input type="checkbox" value="1" id="{{$name}}" {{$value == 1 ? 'checked' : ''}} name="{{$name}}">
            <div class="state">
                <label for="{{$name}}">{{$label}}</label>
            </div>
        </div>

        <div class="error-msg"></div>
    </div>
</div>
