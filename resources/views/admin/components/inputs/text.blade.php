@php
    $colLeft = empty($colLeft) ? 4 : $colLeft;
    if($colLeft == 12){
        $colRight = 12;
    }
    $typeTxt = (isset($type) && in_array($type, ['text', 'number'])) ? $type : 'text';
@endphp
<div class="row form-group {{$className ?? ''}} fg-{{$name}}">
    <div class="col-md-{{$colLeft}} {{$labelAlign??'text-left'}}">
        <label>
            {{$label}}
            
            @isset ($required)
                <span class="text-danger"> ※</span>
            @endisset
        </label>
    </div>
    <div class="col-md-{{$colRight ?? 12 - $colLeft}}">
        <input type="{{$typeTxt}}" class="form-control {{$classObj ?? ''}}" {{$readonly ?? ''}} name="{{$name??''}}" value="{{$value??''}}" placeholder="{{$placeholder??''}}">
        <div class="error-msg"></div>
    </div>
</div>