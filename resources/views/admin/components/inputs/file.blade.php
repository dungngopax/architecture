@php
    $colLeft = empty($colLeft) ? 4 : $colLeft;
@endphp
<div class="row form-group {{$className ?? ''}} fg-{{$name}}">
    <div class="col-md-{{$colLeft}} {{$labelAlign??'text-left'}}">
        <label>
            {{$label}}
            
            @isset ($required)
                <span class="text-danger"> ※</span>
            @endisset
        </label>
    </div>
    <div class="col-md-{{12 - $colLeft}}">
    <input type="file" name="{{$name}}" class="{{$classInputFile ?? ''}}" data-target-img-preview="{{$targetImgPreview ?? ''}}">
        <div class="error-msg"></div>
    </div>
</div>