@php
    $seg2 = Request::segment(2);
@endphp

<div class="main-menu">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 pl-2">
                <ul class="nav">
                    <li class="nav-item {{ ($seg2  == 'news-events' ) ? 'active' : '' }}">
                        <a class="nav-link" href="{{url('admin/news-events')}}"><i class="fas fa-eye"></i> News - Events</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
