<div class="box-body table-responsive no-padding">
    <table class="table table-hover">
        <tr>
            <th>ID</th>
            <th>Category</th>
            <th>Title</th>
            <th class="text-center">Position</th>
            <th class="text-center">Status</th>
            <th></th>
        </tr>

        @if (count($records) > 0)
            @foreach ($records as $item)
                <tr>
                    <td>{{$item->id_display}}</td>
                    <td>{{$item->newsEventsCats->title}}</td>
                    <td>{{$item->title}}</td>
                    <td class="text-center">{{$item->position}}</td>
                    <td class="text-center">{!!$item->status_display!!}</td>
                    <td class="text-center">
                        <span data-action="{{$mod}}/{{$item->id}}" data-target="#modal-edit-{{$mod}}" class="label pointer label-success ajax-modal">Edit</span>
                        <span data-action="{{$mod}}/{{$item->id}}" data-target="#modal-edit-{{$mod}}" class="label pointer label-danger delete-item ml-5px">Delete</span>
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="5" class="text-center">No data</td>
            </tr>
        @endif
    </table>
</div>
<div class="text-center wrap-pagination">{{$records->links()}}</div>