<div class="box-header">

    <div class="row">
        <div class="col-md-9">
            <div class="row">
                <div class="col-md-1 label-filter">Search</div>
                <div class="col-md-11">
                    <form action="">
                        <div class="row">
                            @if (count($cats) > 0)
                                <div class="col-md-4">
                                    <select class="form-control auto-submit-form" name="cat_id">
                                        <option value="">Filter by categories</option>
                                        @foreach ($cats as $item)
                                    <option value="{{$item->id}}" {{(request()->cat_id == $item->id) ? 'selected' : ''}}>{{$item->title}}</option>
                                        @endforeach
                                    </select>
                                </div>    
                            @endif

                            <div class="col-md-3">
                                <div class="input-group input-group-sm" style="width: 250px;">
                                    <input type="text" name="q" class="form-control pull-right" value="{{request()->q}}" placeholder="Search">
                                    <div class="input-group-btn">
                                        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        
                    </form>
                </div>
            </div>
        </div>

        <div class="col-md-3 text-right">
            <button type="button" data-action="{{$mod}}/0" data-target="#modal-edit-{{$mod}}" class="ajax-modal btn btn-success"><i class="fa fa-plus"></i> Add New</button>
        </div>
    </div>

</div>