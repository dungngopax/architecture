<!DOCTYPE html>
<html lang="en-US">

<head>
    <meta charset="utf-8">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title', 'Trang chủ') - Transport</title>
	<meta name="author" content="Nile-Theme">
	<meta name="robots" content="index follow">
	<meta name="googlebot" content="index follow">
	<meta http-equiv="content-type" content="text/html; charset=utf-8">
	<meta name="keywords" content="cargo, clean, contractor, corporate, freight, industry, localization, logistics, modern, shipment, transport, transportation, truck, trucking">
	<meta name="description" content="Transportation and Logistics Responsive HTML5 Template">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- google fonts -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800%7CPoppins:300i,300,400,500,600,700,400i,500%7CDancing+Script:700%7CDancing+Script:700%7CGreat+Vibes:400%7CPoppins:400%7CDosis:800%7CRaleway:400,700,800&amp;subset=latin-ext" rel="stylesheet">
	<!-- animate -->
	<link rel="stylesheet" href="{{asset('assets/css/animate.css')}}" />
	<!-- owl Carousel assets -->
	<link href="{{asset('assets/css/owl.carousel.css')}}" rel="stylesheet">
	<link href="{{asset('assets/css/owl.theme.css')}}" rel="stylesheet">
	<!-- bootstrap -->
	<link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
	<!-- hover anmation -->
	<link rel="stylesheet" href="{{asset('assets/css/hover-min.css')}}">
	<!-- flag icon -->
	<link rel="stylesheet" href="{{asset('assets/css/flag-icon.min.css')}}">
	<!-- main style -->
	<link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
	<!-- elegant icon -->
	<link rel="stylesheet" href="{{asset('assets/css/elegant_icon.css')}}">
	<!-- fontawesome  -->
	<link rel="stylesheet" href="{{asset('assets/fonts/font-awesome/css/font-awesome.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('assets/rslider/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('assets/rslider/fonts/font-awesome/css/font-awesome.css')}}">
	<!-- REVOLUTION STYLE SHEETS -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/rslider/css/settings.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/app.css')}}">
</head>

<body>
	@include('front-end.header')
	@include('front-end.search')
    
    @yield('content')

    @include('front-end.footer')

	<!-- jquery library  -->
	<script src="{{asset('assets/js/jquery-3.2.1.min.js')}}"></script>
	<!-- REVOLUTION JS FILES -->
	<script src="{{asset('assets/js/nile-slider.js')}}"></script>
	<script src="{{asset('assets/rslider/js/jquery.themepunch.tools.min.js')}}"></script>
	<script src="{{asset('assets/rslider/js/jquery.themepunch.revolution.min.js')}}"></script>
	<!-- SLIDER REVOLUTION 5.0 EXTENSIONS -->
	<script src="{{asset('assets/rslider/js/extensions/revolution.extension.actions.min.js')}}"></script>
	<script src="{{asset('assets/rslider/js/extensions/revolution.extension.carousel.min.js')}}"></script>
	<script src="{{asset('assets/rslider/js/extensions/revolution.extension.kenburn.min.js')}}"></script>
	<script src="{{asset('assets/rslider/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>
	<script src="{{asset('assets/rslider/js/extensions/revolution.extension.migration.min.js')}}"></script>
	<script src="{{asset('assets/rslider/js/extensions/revolution.extension.navigation.min.js')}}"></script>
	<script src="{{asset('assets/rslider/js/extensions/revolution.extension.parallax.min.js')}}"></script>
	<script src="{{asset('assets/rslider/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
	<script src="{{asset('assets/rslider/js/extensions/revolution.extension.video.min.js')}}"></script>
	<script src="{{asset('assets/js/YouTubePopUp.jquery.js')}}"></script>
	<script src="{{asset('assets/js/owl.carousel.min.js')}}"></script>
	<script src="{{asset('assets/js/imagesloaded.min.js')}}"></script>
	<script src="{{asset('assets/js/custom.js')}}"></script>
	<script src="{{asset('assets/js/popper.min.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
	<script src="{{asset('assets/js/app.js')}}"></script>
</body>
</html>
