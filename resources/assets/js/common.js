(function() {

    //Submit form ajax
    $(document).on("click", ".btn-submit",function(e){
        $(this).submitFormAjax({
            'form' : $( $(this).data('form-target') ),
            'method' : $(this).data('form-method'),
            'error': $(this).data('callback-error'),
            'success': function(res){
                alert('Hệ thống đã ghi nhận thông tin của bạn. Cảm ơn!!!');
                location.reload();
            },
            'autoCloseModal': $(this).data('auto-close-modal'),
        });
    });


    //Submit form ajax
    $(document).on("click", ".ajax-modal",function(e){
        e.stopPropagation();

        var modal = $(this).data('target');

        $(this).submitDataAjax({
            'url' : site_url + $(this).data('action'),
            'method': 'GET',
            'data' : $(this).data('params'),
            'success': function(res){
                $('.modal').modal('hide');
                $(modal).find('.modal-content').html(res.html);
                $(modal).modal();
            }
        });
    });

    //Confirm delete
    $('.delete-item').click(function(){
        if(confirm("Bạn có thật sự muốn xoá item này không?")){
            var id = $(this).data('id');

            $(this).submitDataAjax({
                'url' : site_url + $(this).data('action'),
                'method': 'POST',
                'data' : { _method: 'DELETE', _token: CSRF_TOKEN },
                'success': function(res){
                    location.reload();
                }
            });
        }
    });

    //Auto submit form
    $(".auto-submit-form").change(function() {
        $(this).parents('form').submit();
    });

    $(document).on("click", ".close-img-preview",function(e){
        $(this).hide();
        var parent = $(this).parent();
        parent.find('img').attr('src', urlNoImage);
        parent.find('input[type=hidden]').val('');
    });
    
})();