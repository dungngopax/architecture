var callbacks = $.Callbacks();
(function( $ ){

    //Submit Form Ajax (Modal Popup)
    $.fn.submitFormAjax = function(params) {
        var self = this;
        self.attr('disabled', 'disabled');

        let formObj = params.form;
        formObj.find('.form-group').removeClass('has-error');
        formObj.find('.error-msg').html('');
        
        //Submit ajax
        $.ajax({
            url: formObj.attr('action'),
            type: (params.method === undefined)?'POST':params.method,
            data: formObj.serialize(),
            dataType: 'JSON',
            error: function(response){
                if (params.error) {
                    if (typeof params.error == 'function') {
                        params.error.call(null, response);
                    }else{
                        callbacks.add( eval('document.' + params.error) );
                    }
                }else{
                    var errors = response.responseJSON.errors;
                    for (let key in errors) {
                        // Replace "." degit by "_". Edit by DatHQ
                        formObj.find('.fg-' + key.replace('.', '_')).addClass('has-error').find('.error-msg').html(errors[key]);
                    }

                    //Style Form Tabs
                    if(formObj.parent().hasClass('form-tabs')){
                        formObj.find('.nav-tabs li').each(function(e){
                            var tabTarget = $(this).find('a').attr('href');
                            if($(tabTarget).find('.has-error').length > 0){
                                console.log(tabTarget);
                                console.log(e);
                                $('.nav-tabs li:eq('+e+') a').tab('show');
                                return false;
                            }
                        });
                    }
                }
            },
            success: function (response) {
                console.log(response);
                if(formObj.find(' .btn-close-modal').length > 0){
                    formObj.find('.btn-close-modal').trigger('click');
                }

                if (params.success) {
                    if (typeof params.success == 'function') {
                        params.success.call(null, response);
                    }else{
                        callbacks.add( eval('document.' + params.success) );
                    }

                    return false;
                }

                if(self.data('url-redirect')){
                    location.href = self.data('url-redirect');
                }else{
                    location.reload();
                }
            },
            complete: function(response){
                self.removeAttr('disabled');
                callbacks.fire(response);
            }
        });
    };
    
    //Submit Ajax
    $.fn.submitDataAjax = function(params) {
        
        var self = this;
        self.attr('disabled', 'disabled');

        //Submit ajax
        $.ajax({
            url: params.url,
            type: params.method,
            data: params.data,
            dataType: 'JSON',
            error: function(response){
                if (params.error) {
                    if (typeof params.error == 'function') {
                        params.error.call(null, response);
                    }else{
                        callbacks.add( eval('document.' + params.error) );
                    }
                }
            },
            success: function (response) {
                if (params.success) {
                    if (typeof params.success == 'function') {
                        params.success.call(null, response);
                    }else{
                        callbacks.add( eval('document.' + params.success) );
                    }
                }else{
                    location.reload();
                }
            },
            complete: function(response){
                self.removeAttr('disabled');
                callbacks.fire(response);
            }
        });
    };
})( jQuery );

document.getNumber = function(str){
    if(!str){
        return 0;
    }
    
    str = str.trim();
    number = parseInt(str.replace(/đ|,/g, ""));
    return isNaN(number) ? 0 : number;
};