<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ServicesCats;
use App\Models\Services;

class ServicesController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $category = '')
    {
        $query = Services::where('status', 1)->orderBy('position', 'desc');
        $cat = null;

        if(!empty($category)){
            $cat = ServicesCats::where('slug', $category)->first();
            if(empty($cat)){
                return redirect('/');
            }else{
                $query = $query->where('cat_id', $cat->id);
            }
        }

        $records = $query->paginate(21);

        return view('front-end.services.index', [
            'records' => $records,
            'cat' => $cat
        ]);
    }

    public function detail(Request $request, $category = '', $slug = ''){
        $cat = ServicesCats::where('slug', $category)->first();
        if(empty($cat)){
            return redirect('/');
        }

        $record = Services::where('slug', $slug)->first();
        if(empty($record)){
            return redirect('/');
        }

        $cats = ServicesCats::where('status', 1)->orderBy('position', 'desc')->get();

        $recordsRel = Services::where('id', '<>', $record->id)->where('status', 1)->orderBy('position', 'desc')->limit(10)->get();

        return view('front-end.services.detail', [
            'record' => $record,
            'recordsRel' => $recordsRel,
            'cat' => $cat,
            'cats' => $cats
        ]);
    }
}
