<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\NewsEventsCats;
use App\Models\NewsEvents;

class NewsEventsController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $category = '')
    {
        $query = NewsEvents::where('status', 1)->orderBy('position', 'desc');
        $cat = null;

        if(!empty($category)){
            $cat = NewsEventsCats::where('slug', $category)->first();
            if(empty($cat)){
                return redirect('/');
            }else{
                $query = $query->where('cat_id', $cat->id);
            }
        }

        $records = $query->paginate(21);

        return view('front-end.news-events.index', [
            'records' => $records,
            'cat' => $cat
        ]);
    }

    public function detail(Request $request, $category = '', $slug = ''){
        $cat = NewsEventsCats::where('slug', $category)->first();
        if(empty($cat)){
            return redirect('/');
        }

        $record = NewsEvents::where('slug', $slug)->first();
        if(empty($record)){
            return redirect('/');
        }

        $cats = NewsEventsCats::where('status', 1)->orderBy('position', 'desc')->get();

        $recordsRel = NewsEvents::where('id', '<>', $record->id)->where('status', 1)->orderBy('position', 'desc')->limit(10)->get();

        return view('front-end.news-events.detail', [
            'record' => $record,
            'recordsRel' => $recordsRel,
            'cat' => $cat,
            'cats' => $cats
        ]);
    }
}
