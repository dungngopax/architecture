<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Helpers\Common;
use Illuminate\Support\Facades\Storage;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function cleanData($txt){
        return Common::cleanData($txt);
    }

    //Ex: $this->updateFileRecord($params['thumbnail'], $record->thumbnail, 'products');
    public function updateFileRecord(&$paramName, $recordName, $folder){
        if(empty($paramName)){
            if(!empty($recordName)){ //Delete file old
                Storage::delete($recordName);
            }
        }else{
            if(Storage::exists($paramName)){
                if( $paramName != $recordName ){//Add new file
                    $desFolder = $folder . '/' . date("Y") . '/' . date("m") . '/' . basename($paramName);

                    if(Storage::move($paramName, $desFolder)){
                        Storage::delete($recordName);
                    }

                    $paramName = $desFolder;
                }
            }else{
                $paramName = '';
            }
        }
    }

}
