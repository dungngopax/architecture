<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Illuminate\Http\Request;
use App\Http\Requests\Auth\StoreProfile;
use App\User;

class ProfileController extends Controller
{
    public function index(StoreProfile $request){
        $user = Auth::user();
        
        $params = $request->all();

        if(!empty($params['password'])){
            $params['password'] = bcrypt($params['password']);
        }
        $user->fill($params);

        if($user->save()){
            return response()->json(['status'=>200]);    
        }

        return response()->json(['status'=>500]);
    }
}