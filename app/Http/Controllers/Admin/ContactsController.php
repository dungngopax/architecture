<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Contacts;

use App\Http\Requests\StoreContacts;

class ContactsController extends Controller
{
    public $title = 'Contacts';
    public $mod = 'contacts';

    public function index(Request $request){
        $records = Contacts::search($request)->paginate(20);

        return view('admin.'.$this->mod.'.index', [
            'records' => $records,
            'title' => $this->title,
            'mod' => $this->mod
        ]);
    }

    public function show($id){
        $record = empty($id) ? new Contacts : Contacts::find($id);

        $html = (string)view('admin.'.$this->mod.'.edit', [
            'title' => $this->title,
            'mod' => $this->mod,
            'record' => $record,
            'status' => (array)config('constants.status'),
        ]);

        return response()->json(['html' => $html]);
    }

    public function update(StoreContacts $request){
        $record = empty($request->id) ? new Contacts : Contacts::find($request->id);

        $params = $request->all();

        $params['ip'] = $request->ip();

        $record->fill($params)->save();

        return response()->json(['st' => 200]);
    }

    public function destroy($id){
        $st = Contacts::find($id)->delete();
        return response()->json(['html' => $st]);
    }
}
