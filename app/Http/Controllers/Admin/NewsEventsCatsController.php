<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\NewsEventsCats;

use App\Http\Requests\StoreNewsEventsCats;

class NewsEventsCatsController extends Controller
{
    public $title = 'News - Events Categories';
    public $mod = 'news-events-cats';

    public function index(Request $request){
        $records = NewsEventsCats::search($request)->paginate(20);

        return view('admin.'.$this->mod.'.index', [
            'records' => $records,
            'title' => $this->title,
            'mod' => $this->mod
        ]);
    }

    public function show($id){
        $record = empty($id) ? new NewsEventsCats : NewsEventsCats::find($id);

        $html = (string)view('admin.'.$this->mod.'.edit', [
            'title' => $this->title,
            'mod' => $this->mod,
            'record' => $record,
            'status' => (array)config('constants.status'),
        ]);

        return response()->json(['html' => $html]);
    }

    public function update(StoreNewsEventsCats $request){
        $record = empty($request->id) ? new NewsEventsCats : NewsEventsCats::find($request->id);

        $params = $request->all();

        $params['slug'] = str_slug($params['title']);

        $record->fill($params)->save();

        return response()->json(['st' => 200]);
    }

    public function destroy($id){
        $st = NewsEventsCats::find($id)->delete();
        return response()->json(['html' => $st]);
    }
}
