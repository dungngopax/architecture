<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ServicesCats;

use App\Http\Requests\StoreServicesCats;

class ServicesCatsController extends Controller
{
    public $title = 'Service Categories';
    public $mod = 'services-cats';

    public function index(Request $request){
        $records = ServicesCats::search($request)->paginate(20);

        return view('admin.'.$this->mod.'.index', [
            'records' => $records,
            'title' => $this->title,
            'mod' => $this->mod
        ]);
    }

    public function show($id){
        $record = empty($id) ? new ServicesCats : ServicesCats::find($id);

        $html = (string)view('admin.'.$this->mod.'.edit', [
            'title' => $this->title,
            'mod' => $this->mod,
            'record' => $record,
            'status' => (array)config('constants.status'),
        ]);

        return response()->json(['html' => $html]);
    }

    public function update(StoreServicesCats $request){
        $record = empty($request->id) ? new ServicesCats : ServicesCats::find($request->id);

        $params = $request->all();

        $params['slug'] = str_slug($params['title']);

        $record->fill($params)->save();

        return response()->json(['st' => 200]);
    }

    public function destroy($id){
        $st = ServicesCats::find($id)->delete();
        return response()->json(['html' => $st]);
    }
}
