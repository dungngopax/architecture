<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Services;
use App\Http\Requests\StoreServices;
use App\Models\ServicesCats;

class ServicesController extends Controller
{
    public $title = 'Services';
    public $mod = 'services';

    public function index(Request $request){
        $records = Services::search($request)->paginate(20);
        $cats = ServicesCats::where('status', 1)->orderBy('position', 'desc')->get();

        return view('admin.'.$this->mod.'.index', [
            'records' => $records,
            'cats' => $cats,
            'title' => $this->title,
            'mod' => $this->mod
        ]);
    }

    public function show($id){
        $record = empty($id) ? new Services : Services::find($id);
        $cats = ServicesCats::where('status', 1)->orderBy('position', 'desc')->select('id', 'title as name')->get()->toArray();

        $html = (string)view('admin.'.$this->mod.'.edit', [
            'title' => $this->title,
            'mod' => $this->mod,
            'record' => $record,
            'cats' => $cats,
            'status' => (array)config('constants.status'),
        ]);

        return response()->json(['html' => $html]);
    }

    public function update(StoreServices $request){
        $record = empty($request->id) ? new Services : Services::find($request->id);

        $params = $request->all();

        if($params['thumbnail']){
            $this->updateFileRecord($params['thumbnail'], $record->thumbnail, $this->mod);
        }

        $params['slug'] = str_slug($params['title']);

        $record->fill($params)->save();

        return response()->json(['st' => 200]);
    }

    public function destroy($id){
        $st = Services::find($id)->delete();
        return response()->json(['html' => $st]);
    }
}
