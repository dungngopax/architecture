<?php
namespace App\Models\Traits;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Helpers\Common;
use App\Helpers\Permissions;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;

trait CommonTrait{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function createdUser()
    {
        return $this->belongsTo('App\User', 'create_user', 'email');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function updatedUser()
    {
        return $this->belongsTo('App\User', 'update_user', 'email');
    }

    public function getIdDisplayAttribute() {
        return str_pad($this->id, 5, "0", STR_PAD_LEFT);
    }

    public function getStatusDisplayAttribute(){
        return empty($this->status) ? '<i class="fa fa-ban"></i>' : '<i class="fa fa-check"></i>';
    }

    public function cleanData($txt){
        return Common::cleanData($txt);
    }

    public function resizeImage($urlImage, $w = 300){
        return Common::resizeImage($urlImage, $w);
    }

    public function hasAllPermission($permissions, $permission){
        return Permissions::hasAllPermission($permissions, $permission, $this);
    }

    public function getThumbnailDisplayAttribute(){
        $parsed = parse_url($this->thumbnail);
        if (empty($parsed['scheme'])) {
            return $this->thumbnail ? Storage::url($this->thumbnail) : 'admin/dist/img/noimage.jpg';
        }
        return $this->thumbnail;
    }
}
