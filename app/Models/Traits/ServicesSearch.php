<?php
namespace App\Models\Traits;

use Illuminate\Http\Request;
use App\Models\Services;

trait ServicesSearch
{
    public function scopeSearch($query, Request $request){

        if(!empty($request->q)){
            $q = '%' . $request->q . '%';
            $query = $query->where('title', 'like', $q);
        }

        if(!empty($request->cat_id)){
            $query = $query->where('cat_id', $request->cat_id);
        }

        $query->orderBy('id', 'desc');
        return $query;
    }
}
