<?php
namespace App\Models\Traits;

use Illuminate\Http\Request;
use App\Models\NewsEventsCats;

trait NewsEventsCatsSearch
{
    public function scopeSearch($query, Request $request){

        if(!empty($request->q)){
            $q = '%' . $request->q . '%';
            $query = $query->where('title', 'like', $q)
                ->orWhere('id', $request->q);
        }

        $query->orderBy('id', 'desc');
        return $query;
    }
}
