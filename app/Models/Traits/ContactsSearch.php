<?php
namespace App\Models\Traits;

use Illuminate\Http\Request;
use App\Models\Contacts;

trait ContactsSearch
{
    public function scopeSearch($query, Request $request){

        if(!empty($request->q)){
            $q = '%' . $request->q . '%';
            $query = $query->where('fullname', 'like', $q)
                ->orWhere('email', 'like', $q)
                ->orWhere('message', 'like', $q);
        }

        $query->orderBy('id', 'desc');
        return $query;
    }
}
