<?php

namespace App\Models;

use App\Models\Base;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\NewsEventsSearch;

class NewsEvents extends Base
{
    use NewsEventsSearch;
    
    protected $table = 'news_events';

    protected $fillable = ["cat_id", "title", "slug", "thumbnail", "content", "position", "status"];

    public function newsEventsCats(){
        return $this->belongsTo('App\Models\NewsEventsCats', 'cat_id');
    }
}
