<?php

namespace App\Models;

use App\Models\Base;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\NewsEventsCatsSearch;

class NewsEventsCats extends Base
{
    use NewsEventsCatsSearch;
    
    protected $table = 'news_events_cats';

    protected $fillable = ["title", "slug", "position", "status"];

}
