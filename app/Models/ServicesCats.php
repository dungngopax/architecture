<?php

namespace App\Models;

use App\Models\Base;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\ServicesCatsSearch;

class ServicesCats extends Base
{
    use ServicesCatsSearch;
    
    protected $table = 'services_cats';

    protected $fillable = ["title", "slug", "position", "status"];

}
