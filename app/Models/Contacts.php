<?php

namespace App\Models;

use App\Models\Base;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\ContactsSearch;

class Contacts extends Base
{
    use ContactsSearch;
    
    protected $table = 'contacts';

    protected $fillable = ["fullname", "email", "message", "ip"];
}
