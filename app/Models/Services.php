<?php

namespace App\Models;

use App\Models\Base;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\ServicesSearch;

class Services extends Base
{
    use ServicesSearch;
    
    protected $table = 'services';

    protected $fillable = ["cat_id", "title", "slug", "thumbnail", "content", "position", "status"];

    public function servicesCats(){
        return $this->belongsTo('App\Models\ServicesCats', 'cat_id');
    }
}
