<?php
namespace App\Helpers;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Request;
use App\Helpers\DateHelper;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use App\Models\TmpWareHouse;
use Spatie\Permission\Models\Permission;

class Common{

    public static function resizeImage($urlImage, $w = 300){

        if(empty($urlImage)){
            $urlImage = 'admin/dist/img/noimage.jpg';
        }else{
            if(strpos($urlImage, 'storage') != false){
                $urlImage = str_replace('/storage/', '', $urlImage);
            }
    
            $exists = Storage::exists($urlImage);
            if(!$exists){
                $urlImage = 'admin/dist/img/noimage.jpg';
            }else{
                $urlImage = trim(Storage::url($urlImage), '/');
            }
        }

        $image = Image::cache(function($image) use ($urlImage, $w) {
            $image->make($urlImage)->resize($w, null, function ($constraint) {
                $constraint->aspectRatio();
            });
        }, 60*24*30, true);//30 days

        return $image->encode('data-url');
    }

    public static function cleanData($txt){
        return strip_tags(trim($txt));
    }

    public static function convertArrayToString($array){
        if(empty($array)){
            return "[]";
        }

        $txt = "[";
        foreach ($array as $item) {
            $txt .= '"' . $item . '",';
        }

        return trim($txt, ",") . "]";
    }

    public static function convertDateExcelToDateDb($date, $format = 'Y-m-d H:i:s'){
        if(empty($date)){
            return null;
        }

        $date = str_replace('/', '-', $date);
        $date = str_replace(': ', ':', $date);
        return date($format, strtotime($date));
    }

    // Trạng thái giao hàng
    public static function countStatusOrder($status){
        return number_format(TmpWareHouse::where('tt_giao_hang', $status)->count());
    }

    // Mã hàng (Chưa giao hàng)
    public static function countCodeOrder($code){
        return number_format(TmpWareHouse::where('ma_hang', $code)->where('tt_giao_hang', 0)->count());
    }

    public static function isArrayMulti($array) {
        $rv = array_filter($array,'is_array');
        if(count($rv)>0) return true;
        return false;
    }

    public static function hasPermission($model, $permission, $isRole = true) {
        if($isRole){
            return $model->hasPermissionTo($permission) ? 'true' : 'false';
        }

    }

    public static function renderPermissions($permissions, $model, $isRole = true){
        $res = [];
        if($permissions){
            foreach($permissions as $label => $item){
                $permission = Common::renderRecursivePermissions(str_slug($label), $label, $item, $model, $isRole);
                $permission->expanded = true;
                $res[] = (object)$permission;
            }
        }
        return $res;
    }

    public static function renderRecursivePermissions($slugParent, $label, $items, $model, $isRole){
        if(empty($items) || !is_array($items)){
            $slug = $slugParent . '-' .str_slug($items);
            $permission = ['id' => $slug, 'text' => $items];

            if($isRole){
                $chk_permission = Permission::where('name', $slug)->first();
                if(!$chk_permission){
                    $chk_permission = Permission::create(['name' => $slug]);
                }

                $permission['checked'] = $model->hasPermissionTo($slug);
            }

            return (object)$permission;
        }

        $slug = str_slug($label);
        $slugParent = ($slugParent != $slug) ? $slugParent . '-' .$slug : $slug;
        $permission = ['id' => $slugParent, 'text' => $label];
        foreach ($items as $key => $item) {
            $permission['items'][] = self::renderRecursivePermissions($slugParent, $key, $item, $model, $isRole);
        }

        return (object)$permission;
    }

    public static function slugFilename($filename){
        $info = pathinfo(storage_path() . $filename);
        $ext = $info['extension'];

        $arr = explode('.', trim($filename));
        if(empty($arr) || count($arr) < 2){
            return '';
        }

        return str_slug($arr[0]) . '.' . $arr[1];
    }
}
