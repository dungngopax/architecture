<?php
namespace App\Helpers;

use Carbon\Carbon;

class DateHelper{

    //get date format Y/m/d (day of the week) H:i
    public static function getDateAndDayOfTheWeek($date,$time){
        $dayOfTheWeek = self::$day_of_the_week[date('w',strtotime($date))];
        $date = $date." ".$time;
        return date('Y/m/d ('.$dayOfTheWeek.') H:i',strtotime($date));

    }

    // Check date string is valid
    public static function validateDate($date, $format = 'Y-m-d')
    {
        $d = \DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    public static function formatDate($date, $format = 'Y/m/d') {
        if($date) {
            //$date = str_replace('/', '-', $date);
            if(self::validateDate($date)) {
                return Carbon::parse($date)->format($format);
            }

            return $date;
        }
    }

    public static function daysInMonth($year = NULL, $month = NULL) {
        if($year && $month) {
            return date('t', mktime(0, 0, 0, $month, 1, $year));
        }
        return 31;
    }

    public static function currentDate($format = "Y-m-d H:i:s") {
        $date = date_create();
        return date_format($date, $format);
    }

    public static function tomorrowDate(){
        return Carbon::tomorrow()->toDateString();
    }

    public static function nowDate(){
        return Carbon::now()->toDateString();
    }

    public static function convertDateViToDateDb($date, $delimiter = '-'){
        $date = explode($delimiter, $date);

        if(count($date) != 3){
            return '';
        }
        return $date[2] . '-' . $date[1] . '-' . $date[0];
    }

    public static function printDate($date, $format = 'd-m-Y H:i:s'){
        if(empty($date)){
            return '';
        }

        return date($format, strtotime($date));
    }

    public static function betweenDate($date){
        if(empty($date)){
            return '';
        }

        return [ $date.' 00:00:00', $date.' 23:59:59' ];
    }

    public static function subtractDate($start_time, $type = 'days', $finish_time = null){
        if(empty($finish_time)){
            $finish_time = Carbon::now();
        }

        if($type == 'days'){
            return $start_time->diffInDays($finish_time, false);
        }

        return $start_time->diffInMonths($finish_time, false);
    }
}