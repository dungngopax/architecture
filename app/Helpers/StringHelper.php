<?php

namespace App\Helpers;
use Illuminate\Support\Str;

class StringHelper {
    public static function truncate($string, $length = 150, $strip_tags = true) {
        return $strip_tags ? strip_tags(Str::words($string, $length)) : Str::words($string, $length);
    }
}