<?php
namespace App\Helpers;

use App\Models\Products;
use App\Models\ProductsComponents;
use App\Models\BranchesProducts;

class Product{

    public static function checkInventoryCart($type = 'orders', $itemsCart){
        $dataError = [];

        if($itemsCart){
            foreach($itemsCart as $item){

                $inventory = self::getInventoryByBranch($item->id);

                if($type == 'orders'){
                    $sets_register = self::getSetsInventory($item->id);
                    $inventory = max($inventory, $sets_register);
                }
                
                if($inventory < $item->quantity){
                    $item->inventory = $inventory;
                    $dataError[] = $item;
                }
            }
        }

        return $dataError;
    }

    public static function getItemCartByProductId($products_id, $itemsCart){
        if($itemsCart){
            foreach($itemsCart as $item){
                if($item->id == $products_id){
                    return $item;
                }
            }
        }

        return null;
    }

    public static function checkAddToCart($product, $qty, $type = 'orders'){
        $inventory = self::getInventoryByBranch($product->id);
    
        if( $inventory >= $qty ){
            return true;
        }
    
        if($type == 'invoices'){
            return false;
        }

        if( ($product->sets_register + $qty) > ($product->number_of_sets + $inventory) ){
            return false;
        }
    
        return true;
    }

    public static function getInventoryByBranch($products_id, $branches_id = 0){
        $product = Products::find($products_id);

        if(!$product){
            return 0;
        }

        if(!$branches_id){
            $branchActive = json_decode($_COOKIE['branch_active']);
            $branches_id = $branchActive->id;
        }

        $branchProduct = BranchesProducts::where('branches_id', $branches_id)
                        ->where('in_branch', 1)
                        ->where('products_id', $products_id)
                        ->first();

        return $branchProduct ? $branchProduct->inventory : 0;
    }

    public static function getSetsInventory($products_id){
        $product = Products::find($products_id);
        return $product ? ($product->number_of_sets - $product->sets_register) : 0;
    }

    //Update Inventory number
    public static function updateInventoryNumber($product_types_id, $products_id){
        $product = Products::find($products_id);

        if($product){

            if($product_types_id == 1){//Combo - Dong goi

                $components = ProductsComponents::with('products')->where('products_id_parent', $products_id)->get();

                if(count($components) > 0){
                    $inventory_number = 999999;
                    $components = $components->map(function ($item) use(&$inventory_number) {
                        $item->inventory_number = $item->products->inventory_number;
    
                        $number_sale = 0;
                        if($item->qty > 0){
                            $number_sale = max(intval(floor($item->products->inventory_number / $item->qty)), 0);
                        }
                        
                        $inventory_number = min($number_sale, $inventory_number);
                        return $item;
                    });

                    $product->inventory_number = $inventory_number;
                }else{
                    $product->inventory_number = 0;
                }
                
            }else{//Check single product
                $product->inventory_number = BranchesProducts::where('products_id', $products_id)->sum('inventory');
            }

            if($product->save()){
                self::updateWarningOutOfStock($products_id);
                return $product->inventory_number;
            }
        }

        return false;
    }

    //Warning product is out of stock
    public static function updateWarningOutOfStock($products_id){
        $product = Products::find($products_id);

        if(!$product){
            return false;
        }

        $record = BranchesProducts::where('products_id', $products_id)->whereColumn('inventory', '<=', 'min_inventory')->first();
        $product->warning_out_of_stock = $record ? 1 : 0;

        return $product->save();
    }
}
