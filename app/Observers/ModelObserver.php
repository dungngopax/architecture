<?php
namespace App\Observers;

use App\Models\Base;
use Illuminate\Support\Facades\Auth;

class ModelObserver 
{
	/**
     * Listen to the Model creating event.
     *
     * @param  Base  $model
     * @return void
     */
	public function creating(Base $model)
	{
		if(Auth::user() && !$model->create_user && !$model->update_user) 
		{
			$model->create_user = Auth::user()->email;
			$model->update_user = Auth::user()->email;
		}
	}

	/**
     * Listen to the Model updating event.
     *
     * @param  Base  $model
     * @return void
     */
	public function updating(Base $model)
	{
		if(Auth::user()) {
			$model->update_user = Auth::user()->email;
		}
	}

	/**
     * Listen to the Model deleting event.
     * 
     * Deleting is pretty different, We have to manual update `delete_user` column
     * Because soft delete trait is only update 2 columns: `updated_at` and `deleted_at`.
     *
     * @param  Base  $model
     * @return void
     */
	public function deleting(Base $model)
	{
		if(Auth::user()) {
			$query = $model->newQueryWithoutScopes()->where($model->getKeyName(), $model->getKey());
			$query->update(['delete_user' => Auth::user()->username]);
		}
	}
}