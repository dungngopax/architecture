/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

var callbacks = $.Callbacks();
(function ($) {

    //Submit Form Ajax (Modal Popup)
    $.fn.submitFormAjax = function (params) {
        var self = this;
        self.attr('disabled', 'disabled');

        var formObj = params.form;
        formObj.find('.form-group').removeClass('has-error');
        formObj.find('.error-msg').html('');

        //Submit ajax
        $.ajax({
            url: formObj.attr('action'),
            type: params.method === undefined ? 'POST' : params.method,
            data: formObj.serialize(),
            dataType: 'JSON',
            error: function error(response) {
                if (params.error) {
                    if (typeof params.error == 'function') {
                        params.error.call(null, response);
                    } else {
                        callbacks.add(eval('document.' + params.error));
                    }
                } else {
                    var errors = response.responseJSON.errors;
                    for (var key in errors) {
                        // Replace "." degit by "_". Edit by DatHQ
                        formObj.find('.fg-' + key.replace('.', '_')).addClass('has-error').find('.error-msg').html(errors[key]);
                    }

                    //Style Form Tabs
                    if (formObj.parent().hasClass('form-tabs')) {
                        formObj.find('.nav-tabs li').each(function (e) {
                            var tabTarget = $(this).find('a').attr('href');
                            if ($(tabTarget).find('.has-error').length > 0) {
                                console.log(tabTarget);
                                console.log(e);
                                $('.nav-tabs li:eq(' + e + ') a').tab('show');
                                return false;
                            }
                        });
                    }
                }
            },
            success: function success(response) {
                console.log(response);
                if (formObj.find(' .btn-close-modal').length > 0) {
                    formObj.find('.btn-close-modal').trigger('click');
                }

                if (params.success) {
                    if (typeof params.success == 'function') {
                        params.success.call(null, response);
                    } else {
                        callbacks.add(eval('document.' + params.success));
                    }

                    return false;
                }

                if (self.data('url-redirect')) {
                    location.href = self.data('url-redirect');
                } else {
                    location.reload();
                }
            },
            complete: function complete(response) {
                self.removeAttr('disabled');
                callbacks.fire(response);
            }
        });
    };

    //Submit Ajax
    $.fn.submitDataAjax = function (params) {

        var self = this;
        self.attr('disabled', 'disabled');

        //Submit ajax
        $.ajax({
            url: params.url,
            type: params.method,
            data: params.data,
            dataType: 'JSON',
            error: function error(response) {
                if (params.error) {
                    if (typeof params.error == 'function') {
                        params.error.call(null, response);
                    } else {
                        callbacks.add(eval('document.' + params.error));
                    }
                }
            },
            success: function success(response) {
                if (params.success) {
                    if (typeof params.success == 'function') {
                        params.success.call(null, response);
                    } else {
                        callbacks.add(eval('document.' + params.success));
                    }
                } else {
                    location.reload();
                }
            },
            complete: function complete(response) {
                self.removeAttr('disabled');
                callbacks.fire(response);
            }
        });
    };
})(jQuery);

document.getNumber = function (str) {
    if (!str) {
        return 0;
    }

    str = str.trim();
    number = parseInt(str.replace(/đ|,/g, ""));
    return isNaN(number) ? 0 : number;
};

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(2);
__webpack_require__(4);
__webpack_require__(5);
module.exports = __webpack_require__(6);


/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(0);
__webpack_require__(3);

/***/ }),
/* 3 */
/***/ (function(module, exports) {

(function () {

    //Submit form ajax
    $(document).on("click", ".btn-submit", function (e) {
        $(this).submitFormAjax({
            'form': $($(this).data('form-target')),
            'method': $(this).data('form-method'),
            'error': $(this).data('callback-error'),
            'success': $(this).data('callback-success'),
            'autoCloseModal': $(this).data('auto-close-modal')
        });
    });

    //Submit form ajax
    $(document).on("click", ".ajax-modal", function (e) {
        e.stopPropagation();

        var modal = $(this).data('target');

        $(this).submitDataAjax({
            'url': site_url + $(this).data('action'),
            'method': 'GET',
            'data': $(this).data('params'),
            'success': function success(res) {
                $('.modal').modal('hide');
                $(modal).find('.modal-content').html(res.html);
                $(modal).modal();
            }
        });
    });

    //Confirm delete
    $('.delete-item').click(function () {
        if (confirm("Bạn có thật sự muốn xoá item này không?")) {
            var id = $(this).data('id');

            $(this).submitDataAjax({
                'url': site_url + $(this).data('action'),
                'method': 'POST',
                'data': { _method: 'DELETE', _token: CSRF_TOKEN },
                'success': function success(res) {
                    location.reload();
                }
            });
        }
    });

    //Auto submit form
    $(".auto-submit-form").change(function () {
        $(this).parents('form').submit();
    });

    $(document).on("click", ".close-img-preview", function (e) {
        $(this).hide();
        var parent = $(this).parent();
        parent.find('img').attr('src', urlNoImage);
        parent.find('input[type=hidden]').val('');
    });
})();

/***/ }),
/* 4 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 5 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 6 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ })
/******/ ]);