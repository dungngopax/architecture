<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Helpers\Common;
use Illuminate\Support\Facades\Storage;
use App\Helpers\DateHelper;

Route::prefix('admin')->name('admin.')->middleware(['auth'])->group(function () {
    Route::resource('news-events-cats', 'Admin\NewsEventsCatsController');
    Route::resource('news-events', 'Admin\NewsEventsController');
    
    Route::resource('services-cats', 'Admin\ServicesCatsController');
    Route::resource('services', 'Admin\ServicesController');

    Route::resource('contacts', 'Admin\ContactsController');

    Route::post('upload-file', 'Admin\FileController@uploadTmp');
    Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout')->name('logout');
});

Auth::routes();

Route::get('/', 'HomeController@index');
Route::get('/gioi-thieu', 'AboutUsController@index');

Route::get('/dich-vu/{category?}', 'ServicesController@index');
Route::get('/dich-vu/{category?}/{slug?}', 'ServicesController@detail');

Route::get('/tin-tuc-su-kien/{category?}', 'NewsEventsController@index');
Route::get('/tin-tuc-su-kien/{category?}/{slug?}', 'NewsEventsController@detail');

Route::get('/lien-he', 'ContactUsController@index');