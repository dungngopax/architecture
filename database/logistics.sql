-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 09, 2018 at 11:49 PM
-- Server version: 5.7.20
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `logistics`
--

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(10) UNSIGNED NOT NULL,
  `fullname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci,
  `ip` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `create_user` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `update_user` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `delete_user` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `fullname`, `email`, `message`, `ip`, `create_user`, `update_user`, `created_at`, `updated_at`, `deleted_at`, `delete_user`) VALUES
(1, 'Dung Ngo', 'dung.ngofs@gmail.com', 'Message', '127.0.0.1', 'admin@gmail.com', 'admin@gmail.com', '2018-11-09 15:56:53', '2018-11-09 16:02:16', NULL, NULL),
(2, 'Lan Thao', 'lanthao@gmail.com', '.....', '127.0.0.1', 'admin@gmail.com', 'admin@gmail.com', '2018-11-09 16:05:52', '2018-11-09 16:05:52', NULL, NULL),
(3, 'Susu', 'susu@gmail.com', 'asd', '127.0.0.1', 'admin@gmail.com', 'admin@gmail.com', '2018-11-09 16:06:28', '2018-11-09 16:06:28', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `news_events`
--

CREATE TABLE `news_events` (
  `id` int(10) UNSIGNED NOT NULL,
  `cat_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `thumbnail` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `position` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `create_user` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `update_user` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `delete_user` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `news_events`
--

INSERT INTO `news_events` (`id`, `cat_id`, `title`, `slug`, `thumbnail`, `content`, `position`, `status`, `create_user`, `update_user`, `created_at`, `updated_at`, `deleted_at`, `delete_user`) VALUES
(1, 2, 'Long Don’t Spend Time Beating On A Wall, Hoping To Trans', 'long-dont-spend-time-beating-on-a-wall-hoping-to-trans', 'news-events/2018/11/5be5a8c838d27.jpg', 'Water Curing Is One Of The Most Traditional Ways To Seal Moisture From The Newly Laid Walls. This Is A Time Consuming Process And Hence Delays The Construction Work. The Curing Compounds From The Top Construction Chemicals Manufacturers In India Help Lock The Moisture On The Freshly Laid Concrete Instantly. This Kind Of Compound Is ...\r\n\r\nWhere can I get some?\r\nMauris fermentum porta sem, non hendrerit enim bibendum consectetur. Fusce diam est, porttitor vehicula gravida at, accumsan bibendum tincidunt imperdiet. Maecenas quis magna faucibus, varius ante sit amet, varius augue. Praesent aliquam, augue ac pulvinar accumsan\r\n\r\nThere are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don’t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn’t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.', 0, 1, '', 'admin@gmail.com', '2018-11-09 05:47:17', '2018-11-09 08:33:30', NULL, NULL),
(2, 2, 'Long Don’t Spend Time Beating On A Wall, Hoping To Trans', 'long-dont-spend-time-beating-on-a-wall-hoping-to-trans', 'news-events/2018/11/5be5a8e9ceaff.jpg', 'Water Curing Is One Of The Most Traditional Ways To Seal Moisture From The Newly Laid Walls. This Is A Time Consuming Process And Hence Delays The Construction Work. The Curing Compounds From The Top Construction Chemicals Manufacturers In India Help Lock The Moisture On The Freshly Laid Concrete Instantly. This Kind Of Compound Is ...\r\n\r\nWhere can I get some?\r\nMauris fermentum porta sem, non hendrerit enim bibendum consectetur. Fusce diam est, porttitor vehicula gravida at, accumsan bibendum tincidunt imperdiet. Maecenas quis magna faucibus, varius ante sit amet, varius augue. Praesent aliquam, augue ac pulvinar accumsan\r\n\r\nThere are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don’t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn’t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc. 2', 1, 1, 'admin@gmail.com', 'admin@gmail.com', '2018-11-09 05:47:17', '2018-11-09 08:34:03', NULL, NULL),
(3, 1, 'Long Don’t Spend Time Beating On A Wall, Hoping To Trans', 'long-dont-spend-time-beating-on-a-wall-hoping-to-trans', 'news-events/2018/11/5be5a8f40e22d.jpg', 'Long Don’t Spend Time Beating On A Wall, Hoping To Trans ...\r\nLong Don’t Spend Time Beating On A Wall, Hoping To Trans ...\r\nLong Don’t Spend Time Beating On A Wall, Hoping To Trans ...', 3, 1, 'admin@gmail.com', 'admin@gmail.com', '2018-11-09 07:28:21', '2018-11-09 08:34:26', NULL, NULL),
(4, 3, 'aasd', 'aasd', NULL, 'saad', 4, 1, 'admin@gmail.com', 'admin@gmail.com', '2018-11-08 14:48:23', '2018-11-09 15:02:20', '2018-11-09 15:02:20', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `news_events_cats`
--

CREATE TABLE `news_events_cats` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `create_user` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `update_user` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `delete_user` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `news_events_cats`
--

INSERT INTO `news_events_cats` (`id`, `title`, `slug`, `position`, `status`, `create_user`, `update_user`, `created_at`, `updated_at`, `deleted_at`, `delete_user`) VALUES
(1, 'News - Events Categories 1', 'news-events-categories-1', 1, 1, 'admin@gmail.com', 'admin@gmail.com', '2018-11-09 05:59:58', '2018-11-09 06:02:33', NULL, NULL),
(2, 'News - Events Categories 2', 'news-events-categories-2', 2, 1, 'admin@gmail.com', 'admin@gmail.com', '2018-11-09 06:02:13', '2018-11-09 06:30:22', NULL, NULL),
(3, 'News - Events Categories 3', 'news-events-categories-3', 3, 1, 'admin@gmail.com', 'admin@gmail.com', '2018-11-09 06:02:22', '2018-11-09 06:30:43', NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(10) UNSIGNED NOT NULL,
  `cat_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `thumbnail` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `position` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `create_user` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `update_user` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `delete_user` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `cat_id`, `title`, `slug`, `thumbnail`, `content`, `position`, `status`, `create_user`, `update_user`, `created_at`, `updated_at`, `deleted_at`, `delete_user`) VALUES
(1, 1, 'Service 1', 'service-1', NULL, NULL, 0, 1, 'admin@gmail.com', 'admin@gmail.com', '2018-11-09 07:25:05', '2018-11-09 07:25:48', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `services_cats`
--

CREATE TABLE `services_cats` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `create_user` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `update_user` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `delete_user` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services_cats`
--

INSERT INTO `services_cats` (`id`, `title`, `slug`, `position`, `status`, `create_user`, `update_user`, `created_at`, `updated_at`, `deleted_at`, `delete_user`) VALUES
(1, 'Service Category 1', 'service-category-1', 1, 1, 'admin@gmail.com', 'admin@gmail.com', '2018-11-09 07:16:24', '2018-11-09 07:16:45', NULL, NULL),
(2, 'Service Category 2', 'service-category-2', 2, 1, 'admin@gmail.com', 'admin@gmail.com', '2018-11-09 07:16:38', '2018-11-09 07:16:49', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `province_id` int(11) DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0: Ngừng hoạt động | 1:Đang hoạt động',
  `create_user` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `update_user` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `email`, `password`, `remember_token`, `birthday`, `image`, `phone`, `province_id`, `address`, `text`, `status`, `create_user`, `update_user`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Admin', 'admin', 'admin@gmail.com', '$2y$10$hAqMy/bPa/dQoXhXcfLI/OggVaK41Y74TIEOqyT0gx3i5u0bImNRW', 'PhrrqDYR2NFiBDOmAoQt7S4CVttg4xc1MZ7DmsL3tsyWmuF7A4cpzUnDzV1Z', '2018-10-10', NULL, '0908946595', 106, 'Long Thanh', NULL, 2, NULL, NULL, NULL, '2018-10-31 20:33:04', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news_events`
--
ALTER TABLE `news_events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news_events_cats`
--
ALTER TABLE `news_events_cats`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services_cats`
--
ALTER TABLE `services_cats`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `province_id` (`province_id`),
  ADD KEY `province_id_2` (`province_id`),
  ADD KEY `province_id_3` (`province_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `news_events`
--
ALTER TABLE `news_events`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `news_events_cats`
--
ALTER TABLE `news_events_cats`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `services_cats`
--
ALTER TABLE `services_cats`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
