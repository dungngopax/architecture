-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 10, 2018 at 12:44 AM
-- Server version: 5.7.20
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `logistics`
--

-- --------------------------------------------------------

--
-- Table structure for table `news_events_cats`
--

CREATE TABLE `news_events_cats` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `create_user` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `update_user` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `delete_user` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `news_events_cats`
--

INSERT INTO `news_events_cats` (`id`, `title`, `slug`, `position`, `status`, `create_user`, `update_user`, `created_at`, `updated_at`, `deleted_at`, `delete_user`) VALUES
(1, 'News - Events Categories 1', 'news-events-categories-1', 1, 1, 'admin@gmail.com', 'admin@gmail.com', '2018-11-09 05:59:58', '2018-11-09 06:02:33', NULL, NULL),
(2, 'News - Events Categories 2', 'news-events-categories-2', 2, 1, 'admin@gmail.com', 'admin@gmail.com', '2018-11-09 06:02:13', '2018-11-09 06:30:22', NULL, NULL),
(3, 'News - Events Categories 3', 'news-events-categories-3', 3, 1, 'admin@gmail.com', 'admin@gmail.com', '2018-11-09 06:02:22', '2018-11-09 06:30:43', NULL, '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `news_events_cats`
--
ALTER TABLE `news_events_cats`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `news_events_cats`
--
ALTER TABLE `news_events_cats`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
